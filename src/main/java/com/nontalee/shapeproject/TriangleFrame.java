/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author nonta
 */
public class TriangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("base:", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(6, 13);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        JLabel lblHeight = new JLabel("height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(6, 33);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        JLabel lblA = new JLabel("a:", JLabel.TRAILING);
        lblA.setSize(50, 20);
        lblA.setLocation(6, 53);
        lblA.setBackground(Color.WHITE);
        lblA.setOpaque(true);
        frame.add(lblA);

        JLabel lblC = new JLabel("c:", JLabel.TRAILING);
        lblC.setSize(50, 20);
        lblC.setLocation(6, 73);
        lblC.setBackground(Color.WHITE);
        lblC.setOpaque(true);
        frame.add(lblC);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 13);
        frame.add(txtBase);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 33);
        frame.add(txtHeight);

        final JTextField txtA = new JTextField();
        txtA.setSize(50, 20);
        txtA.setLocation(60, 53);
        frame.add(txtA);

        final JTextField txtC = new JTextField();
        txtC.setSize(50, 20);
        txtC.setLocation(60, 73);
        frame.add(txtC);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 13);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle base =??? Triangle height =??? a =??? c =???"
                + "area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(700, 100);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    String strA = txtA.getText();
                    String strC = txtC.getText();

                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    double a = Double.parseDouble(strA);
                    double c = Double.parseDouble(strC);

                    Triangle tri = new Triangle(base, height, a, c);

                    lblResult.setText("Triangle base = " + String.format("%.2f", tri.getBase())
                            + "Triangle height = " + String.format("%.2f", tri.getHeight())
                            + "Triangle a = " + String.format("%.2f", tri.getA())
                            + "Triangle c = " + String.format("%.2f", tri.getC())
                            + " area = " + String.format("%.2f", tri.calArea())
                            + " perimeter = " + String.format("%.2f", tri.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtA.setText("");
                    txtC.setText("");
                    txtBase.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}

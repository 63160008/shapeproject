/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.shapeproject;

/**
 *
 * @author nonta
 */
public class Triangle extends Shape {

    private double base;
    private double height; 
    private double a;
    private double c;

    public Triangle(double base, double height,double a,double c) {
        super("Triangle");
        this.base = base;
        this.height = height;
        this.a = a;
        this.c = c;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    public double getA() {
        return a;
    }

    public double getC() {
        return c;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setC(double c) {
        this.c = c;
    }
    

    @Override
    public double calArea() {
        return 0.5 * base * height;
    }

    @Override
    public double calPerimeter() {
        return a+base+c;
    }

}
